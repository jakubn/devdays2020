name: inverse
layout: true
class: inverse

---
layout: false
background-image: url(img/joecocker.jpg)
background-size: contain
background-repeat: no-repeat
background-position: center right

.left-column[

# With a little help from my friends

architecture in a group of anarchists

Jakub Nabrdalik
]

---
layout: false

# whoami

jakub nabrdalik

solution architect & mentor @bottega

Solution Architect, Software Architect, Team Leader, Head of IT, Developer, etc

banking, fintech, ecommerce, telco, energy, startups

From 3 man-month systems, 200+ dev monoliths, 700+ microservices

70+ talks, 200+ workshops

DevoxxPL programme committee, ex WJUG, WGUG leader, etc.

details at [www.linkedin.com/in/jnabrdalik/](https://www.linkedin.com/in/jnabrdalik/)

---

# whatis bottega

getting our clients up to speed

60 seniors (each 10+ years of industry experience)

11,000+ trained developers and attendees

300+ clients in the EU

15 books

400+ articles

---

# 10 years at software houses

Banks, Telcos, Startups, eCommerce, publishers, radios, everything

You need to win a tender (or convince them to trust you)

Analysis, architecture, DB schema design, UI

Without politics: most had 10% estimation error 

With politics: 600% underestimated

On average 2 weeks design

Always 1 lead, sometimes with help for verification

---

# How did we get 10% esimation error

Earlier years: the technology stack

Later years: expertise

--

# I HAVE NEVER DELIVERED A SOLUTION ACCORDING TO INITIAL DESIGN

... and that's by design (Agile for real)

... instead we learn, build relations, and deliver what is necessary

... and that's why our clients come back to us

---

# The problem: distributed systems too large to design

By a single person / team

--

> you often don’t really understand the problem until after the first time you implement a solution. The second time, maybe you know enough to do it right. So if you want to get it right, be ready to start over at least once (Eric Steven Raymond: "The Cathedral and the Bazaar")

---


layout: false
background-image: url(img/Wicked.png)
background-size: contain
background-repeat: no-repeat
background-position: center right

# Wicked problem

.left-column[

en.wikipedia.org/wiki/Wicked_problem

]

???

- The problem is not understood until after the formulation of a solution.
- Wicked problems have no stopping rule.
- Solutions to wicked problems are not right or wrong.
- Every wicked problem is essentially novel and unique.
- Every solution to a wicked problem is a 'one shot operation.'
- Wicked problems have no given alternative solutions.


Every solution to a wicked problem is a "one-shot operation"; because there is no opportunity to learn by trial and error, every attempt counts significantly

Every wicked problem is essentially unique

The choice of explanation determines the nature of the problem's resolution

Solutions to wicked problems are not right or wrong, only better or worse

The problem is not understood until after the formulation of a solution

> because of complex interdependencies, the effort to solve one aspect of a wicked problem may reveal or create other problems [wikipedia]

---


layout: false
background-image: url(img/4fun.jpg)
background-size: contain
background-repeat: no-repeat
background-position: center middle

# The Monolith: 200 devs / 6 years

???

The architect controls, educates, hires/sacks people

Modularity is enforced

Alas, the architect has too much work, people have to little experience, and scope creep eats modules for lunch

The architect leaves

All that's left is drama, development gets really slow

---

layout: false
background-image: url(img/allezon.jpg)
background-size: contain
background-repeat: no-repeat
background-position: center middle

# 700 Microservices

???

700 microservices, 700 people, no architecture overview

Every team has almost total freedom (innovation speed is very high)

Teams are responsible for products (you could sell) - this is *CRUCIAL*. 

Some teams provide SaaS, infra and tools for other teams. Managers are almost mostly technical

Nobody knows how the whole system looks like, nobody needs it apparently

But... we find duplications, bad interfaces, uninformed architecture decisions due to lack of experience (rookie problems) leading to bad APIs, wrong design

Nobody educates, no one shares; Internal conference gets funny

Anarchy

It's not the optimal setup, and the company knows that

---

background-image: url(img/vattenfallnuclear5.jpg)
background-repeat: no-repeat
background-size: contain
background-position: center middle

# Moving to microservices

???

Joining a client, first 2 weeks: defining architecture as-is, and something that could be

> "I've worked here for 2 years and it's the first time I understand how the system works"

An EA "with experience" is hired

EA designs something from the scratch, completely out of touch with tech, not communicating with anyone

Devs rebel, EA moves out

Architecture Guild takes over 

Ok, now what?

---

# The Guild has a completely flat structure

How do we cooperate?

How do we design across products?

Where's the politics?

---

class: center, middle

# Architecture is the shared understanding of people on the project

### How does the guild work | How decisions are made

”Architecture with 800 of My Closest Friends: The Evolution of Comcast’s Architecture Guild” by Jon Moore

https://www.infoq.com/articles/architecture-guild-800-friends/


???

architecture definition: “What is the shared understanding of senior people on the project”

Source of the problem: how the architecture is created in the organisation

---

## Be responsible

Create a git repo for Architecture, give access to everyone

Decide what the guild does & write it down 
- knowledge sharing (diagrams etc.)
- common practices
- technical research, recommendations
- proactive changes to architecture (before development slows down)
- solving urgent issues

---

## Be responsive

Speker of the guild - on duty rotating role that
- ensures people get answers, decisions are made
- calls for working groups when urgent issues arise
- accountable for process and outcomes
- the nice Human Interface of the Guild

---

## Be inclusive

Invite everyone who is interested in architecture 

Accept all by default

--

## Be efficient

The Guild meets at least every sprint, runs through AP and non urgent issues

If there is an urgent issue, the Guild meets right away

For every problem/decision, create a workgroup (everyone interested)

The workgroup does research, publishes RFC in git repo

Everyone can comment, give people at least 1 week to process

When consensus is achieved, publish the recommendation (ADR)

---

## RFC & Architecture Decision Records

```
RFC TEMPLATE
- Context and Problem Statement
- RFC Drivers (a force, facing concern)
- Considered Options
- Pros and Cons of the Options
- Our recommendation
- Positive Consequences
- Negative Consequences
- Links
- Opened questions
```


```
ADR TEMPLATE
- Context: what information did we consider while making this decision?
- Alternatives: what options did we consider and why?
- Decision: what do we recommend?
- Rationale: why did we make that recommendation?
- Consequences: what are the known drawbacks?
```

https://github.com/joelparkerhenderson/architecture_decision_record

---

## Be transparent:

Have an open communication channel (slack/teams/etc)

Get a broadcast email, that forwards to all guild members

Every sprint a newsletter is published
- what the Guild did
- what the Guild is working on
- new RFCs to comment
- new ADRs written
- who is the Speaker of the Guild this sprint
- how you can contact the Guild

---

## Providing Guidance?

“The best architectures, requirements, and designs emerge from self-organizing teams." [Agile manifesto]

People who make decisions, and don’t have to live with the consequences, make stupid decisions.

You probably underestimate peer pressure

???

You need to include people that will implement it (live with its consequences). 
Making a design you don’t need to live with, is a sure way to fail. 
This is also the source of ivory tower architects. 

You are going into microservices to improve productivity, which means you need to give more freedom and power to the people anyway. But giving people power without guidance and monitoring is anarchy, and cities build by anarchists kind of suck.

---

## How decision are made in amorphous groups

> from the Greek a, without, morphé, shape, form

> having no definite form, SHAPELESS; 

> lacking organization or unity

<div style="text-align: center;"> 
	<img src="img/amorphous.png" style="width: 100%;" />
</div>

???

> Whether a material is liquid or solid depends primarily on the connectivity between its elementary building blocks so that solids are characterized by a high degree of connectivity whereas structural blocks in fluids have lower connectivity.

---

layout: false
background-image: url(img/cynefinM.png)
background-size: contain
background-repeat: no-repeat
background-position: center middle

???

## Decision making

When we are in Complex or Chaotic domain (see: Cynefin framework by professor Dave Snowden) but we cannot agree on how to act/probe?

How decisions are made where to put effort with limited resources/time?


How decisions are made where to put effort with limited resources/time?

---

layout: false
background-image: url(img/bazaar2.png)
background-size: contain
background-repeat: no-repeat
background-position: center right

.left-column[
> No quiet, reverent cathedral-building here—rather, the Linux community seemed to resemble a great babbling bazaar of differing agendas and approaches [...] out of which a coherent and stable system could seemingly emerge only by a succession of miracles. 

> The fact that this bazaar style seemed to work, and work well, came as a distinct shock.
]
.right-column[
  
]

???

1997

---

layout: false
background-image: url(img/mythical.jpg)
background-size: contain
background-repeat: no-repeat
background-position: center right

.left-column[

# Too many communications?

> [Fred Brooks in Mythical Man-Month] argued that the complexity and communication costs of a project rise with the square of the number of developers, while work done only rises linearly [...] if Brooks’s Law were the whole picture Linux would be impossible

]

---

# When everyone wants the same, minus ego

> In his discussion of “egoless programming”, [Gerald] Weinberg observed that in shops where developers are not territorial about their code, and encourage other people to look for bugs and potential improvements in it, improvement happens dramatically faster than elsewhere.

--

<div style="text-align: center;"> 
	<img src="img/krwa.jpg" style="width: 100%;" />
</div>

???

But we are territorial: my microservices (product) are not yours!

At the same time, whole system architecture requires cooperation, egoless approach

I have yet to see a corporation without internal politics being played out

> But what is this leadership style and what are these customs? They cannot be based on power relationships

---

# Principle of shared understanding


> [...] connect the selfishness of individual hackers as firmly as possible to difficult ends that can only be achieved by sustained cooperation. 

> [..] the "principle of command" is effectively impossible to apply among volunteers in the anarchist's paradise we call the Internet. To operate and compete effectively, hackers who want to lead collaborative projects have to learn how to recruit and energize effective communities of interest in the mode vaguely suggested by Kropotkin's "principle of understanding"

--

> Linus [...] has shown an acute grasp of Kropotkin’s “principle of shared understanding”

---

# Self motivation

> The success of the open-source community sharpens this question considerably, by providing hard evidence that it is often cheaper and more effective to recruit self-selected volunteers from the Internet than it is to manage buildings full of people who would rather be doing something else.

<div style="text-align: center;"> 
	<img src="img/motivation2.gif" style="width: 100%;" />
</div>

---

class: center, inverse
layout: false
background-image: url(img/team.png)
background-size: contain
background-repeat: no-repeat
background-position: center right

# Treat people like your peers

???

My volleyball coach would adress 11y old: "Gentlemen, you are too good to play like this"

Empowerment & Trust

When you treat people as kids, they behave like kids

Help people grow, and they will love you, and they will feel motivated

Treat them like the best, and they will strive to be meet that expectations


---

# Boring things that need to be done

> Many people (especially those who politically distrust free markets) would expect a culture of self-directed egoists to be fragmented, territorial, wasteful, secretive, and hostile. 

> But this expectation is clearly falsified by (to give just one example) the stunning variety, quality, and depth of Linux documentation. It is a hallowed given that programmers hate documenting; how is it, then, that Linux hackers generate so much documentation? Evidently Linux’s free market in egoboo works better to produce virtuous, other-directed behavior than the massively-funded documentation shops of commercial software producers.

???

Badania żołnierzy USA pokazują że nie walczą dla kraju, pieniędzy czy idei, tylko dla swoich towarzyszy

---

# The role of an architect is helping others

> [...] it’s doubly important that open-source hackers organize themselves for maximum productivity by self-selection—and the social milieu selects ruthlessly for competence. My friend, familiar with both the open-source world and large closed projects, believes that open source has been successful partly because its culture only accepts the most talented 5% or so of the programming population. 

> She spends most of her time organizing the deployment of the other 95%

???

The rock stars can handle themselves. The goal of an architect in a microservice environment is to help the rest 95%. Make their job easy and painless. 

---

# Communication skills

> There is another kind of skill not normally associated with software development which I think is as important as design cleverness to bazaar projects—and it may be more important. A bazaar project coordinator or leader must have good people and communications skills.

We need communication skills to teach (bring others up to speed), share knowledge (principle of common understanding) and finally it is what moves ones agenda forward.

How to improve communication skills?

---

## The nerd-introvert syndrome

Q - How can you tell if a programmer has high social skills

A - During conversation he stares at YOUR shoes

<div style="text-align: center;"> 
	<img src="img/lisp.png" style="width: 50%;" />
</div>

---

## Introvert vs Extravert

 > Extraversion tends to be manifested in outgoing, talkative, energetic behavior, whereas introversion is manifested in more reserved and solitary behavior

> Extraversion is the state of primarily obtaining gratification from outside oneself. Extraverts are energized and thrive off being around other people.

> [Introverts] people whose energy tends to expand through reflection and dwindle during interaction.

--

> Extraversion and introversion are typically viewed as a single continuum, so to be high in one necessitates being low in the other. Jung provides a different perspective and suggests that everyone has both an extraverted side and an introverted side, with one being more dominant than the other. 

---

layout: false
background-image: url(img/sisters2.jpg)
background-size: contain
background-repeat: no-repeat
background-position: center middle

???

My sisters would spend 14h talking with and being around people

I'd spend 10h looking at a computer screen or reading books

That's 10h/d difference in practicing human communication

My sisters love watching drama, social or romance movies

I love sci-fi, action, thriller. I hate watching people interact in tough social context

---

layout: false
background-image: url(img/sisters.jpg)
background-size: contain
background-repeat: no-repeat
background-position: center middle

## It's a matter of practice

???

My sisters used to be much better in social context than me event though I was 5/10 years older.

With time that difference dissapears


---

## Monitoring of the system

Your brain is a parallel processor (unless it is actually a memory system mainly, vide "On Intelligence" by Jeff Hawkins & Sandra Blakeslee).

You can start a monitoring thread. 

It's actually what scientists, religion gurus and poets suggest.


<div style="text-align: center;"> 
	<img src="img/artofbeing.jpg" style="width: 30%;" />
	<img src="img/awarness.jpg" style="width: 30%;" />
	<img src="img/fabula.jpg" style="width: 30%;" />	
</div>

???

Self analysis: "The Art of Being" by Erich Fromm

"Awareness: The Perils and Opportunities of Reality" Anthony De Mello

"Fabula Rasa" by  Edward Stachura


---

layout: false
background-image: url(img/bridge.jpeg)
background-size: contain
background-repeat: no-repeat
background-position: center right

.left-column[

## Monitor emotions

Look for your emotions

"Some evidence for heightened sexual attraction under conditions of high anxiety." Dutton & Aron, 1974

]

???

> 85 male passersby were contacted either on a fear-arousing suspension bridge or a non-fear-arousing bridge by an attractive female interviewer who asked them to fill out questionnaires containing Thematic Apperception Test (TAT) pictures. Sexual content of stories written by Ss on the fear-arousing bridge and tendency of these Ss to attempt postexperimental contact with the interviewer were both significantly greater. No significant differences between bridges were obtained on either measure for Ss contacted by a male interviewer.

---

## How does monitoring work

You realize you feel anger or annoyance

You realize it has nothing to do with the subject of the problem

You do not let it control your behavior, you decide what to do based on intelect instead of emotions

---

## Emotions

You have no control on emerging emotions

You either let them overwhelm you (decide on your actions, strengthen them)

Or you decided to act despite your emotions 

(it's not about blocking your emotions, it's about who controls actions of the body: conscious mind or emotions)

---

## Monitor 2.0

First you do probing on "hey monitor, how do I feel and why"?

Then you put it on autopilot "inform me on high emotions and source". Your brain does it for you without your consciousness.

Just like driving it requires time to internalize.

Remember, you need to check the monitoring system from time to time, or you'll not know when it stops working.

Also, look for logical fallacies in your own words to find hidden emotions and biases

> A fallacy (also called sophism) is the use of invalid or otherwise faulty reasoning, or "wrong moves" in the construction of an argument.

---

## Monitor 3.0

Now start monitoring the energy level of the discussion and other participants as well using empathy

When you see energy levels too low or too high, change strategy

Too low - make space for the quiet types to express themselves, ask open questions more often, ask for their insight, make environment safe

Too high - slow down, calm down yourself, find the root couse of emotions on the other side (often it is fear)

---

layout: false
background-image: url(img/sapolsky.jpeg)
background-size: contain
background-repeat: no-repeat
background-position: center right

## Sympathy

.left-column[

Our emotions and behaviours happen often DESPITE our will

If you want to understand why people behave the way they do, and get some sympathy for the failing human, read this book

Or watch recordings of his "Human Behavioral Biology" lectures at Stanford

https://www.youtube.com/watch?v=NNnIGh9g6fA
]


???

neuroendocrinology professor

> Thus, it is impossible to conclude that a behavior is caused by a gene, a hormone, a childhood trauma, because the second you invoke one type of explanation, you are de facto invoking them all. No buckets. A “neurobiological” or “genetic” or “developmental” explanation for a behavior is just shorthand, an expository convenience for temporarily approaching the whole multifactorial arc from a particular perspective.

---

layout: false
background-image: url(img/nonviolent.jpeg)
background-size: 100%
background-repeat: no-repeat
background-position: center middle

???

Corrective steps after monitoring

Nonviolent Communication - Porozumienie bez przemocy

Consciousness - A set of principles that support living a life of compassion, collaboration, courage, and authenticity.

Language - Understanding how words contribute to connection or distance.

Communication - Knowing how to ask for what you want, how to hear others even in disagreement, and how to move forward towards solutions that work for all.

Means of influence - Sharing “power with others” rather than using “power over others”.

---

class: center

### Provide emotional support 

<iframe width="100%" height="80%" src="https://www.youtube.com/embed/vmsPX-b4lS4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

layout: false
background-image: url(img/puchargrodzisk.jpg)
background-size: contain
background-repeat: no-repeat
background-position: center middle

---

## Example 1


???

Context - my team is very unhappy due to infra k8s and security changes, which make our work difficult. We get a video meeting with "The single person to blame" for all the changes. Anger and frustration in the air. Blood could be drawn.

I start it soft, with focus on what each side really needs and why. I voice my lack of competence (things I don't know about) and ask the other side for helping me understand.

---

layout: false
background-image: url(img/sample1.png)
background-size: contain
background-repeat: no-repeat
background-position: center middle

## Example 1

???

> Dzięki Kuba, w pewnym momencie czułem że może się nie dogadamy, a jakoś Twój wkład to trochę załagodził. Ja miałem trochę cykora bo byłem świadom tych dokumentów a trochę nie podeszłem do tematu rozdzielając proda od reszty. Ogólnie na początku narzekałem że wszystkim działa a wam jako A-Team nie, ale widzę że to właśnie wasz wkład w to wszystko może dać najlepszy efekt

---

class: inverse
layout: false
background-image: url(img/zosia.jpg)
background-size: contain
background-repeat: no-repeat
background-position: center right

# Make people grow

Your goal is not to be the best

Your goal is to help people be better

Move out of the spotlight

---

layout: false
background-image: url(img/Onwriting.jpg)
background-size: contain
background-repeat: no-repeat
background-position: center right

# Learn to write

Go to a creative writing course

Read books about writing

---

layout: false
background-image: url(img/aubrey-plaza.jpg)
background-size: contain
background-size: 75% auto
background-repeat: no-repeat
background-position: center bottom 

# Charm

> "It is not a coincidence that Linus is a nice guy who makes people like him and want to help him. It’s not a coincidence that I’m an energetic extrovert who enjoys working a crowd and has some of the delivery and instincts of a stand-up comic. To make the bazaar model work, it helps enormously if you have at least a little skill at charming people."

---

layout: false
background-image: url(img/linus-finger.png)
background-size: contain
background-size: 75% auto
background-repeat: no-repeat
background-position: center bottom 

# Charm

> "It is not a coincidence that Linus is a nice guy who makes people like him and want to help him. It’s not a coincidence that I’m an energetic extrovert who enjoys working a crowd and has some of the delivery and instincts of a stand-up comic. To make the bazaar model work, it helps enormously if you have at least a little skill at charming people."

---


layout: false
background-image: url(img/trio.jpg)
background-size: contain
background-size: 75% auto
background-repeat: no-repeat
background-position: center middle 

# Charm

> [Charisma definition] "Charisma is a values-based, symbolic, and emotion-laden leader signaling. "

---

layout: false
background-image: url(img/charisma.png)
background-size: contain
background-size: 75% auto
background-repeat: no-repeat
background-position: center middle 

???

---

# Charisma

> In this review, we showed how thinking on charisma has developed over time.

> Contrasting various literatures, we showed how charisma has gone from being conceived as a rather unknown and apparently divine quantity whose nature was revolutionary to a more domesticated version that can be observed in various settings and operationalized to predict various outcomes. 

--

## Summary points

	8. Charisma can be developed.

---

layout: false
background-image: url(img/gosling-pitt.jpeg)
background-size: contain
background-size: 100% auto
background-repeat: no-repeat

### Kids can do it, so can you

---

# You can master communication

In fact, you have your whole life to do it

contact me at: jakubn@gmail.com

twitter: @jnabrdalik

---

layout: false
background-image: url(img/lobotomy.jpeg)
background-size: contain
background-size: 90% auto
background-repeat: no-repeat

# Transorbital lobotomy

James W. Watts & Walter Freeman 

by 1951 over 18,608 individuals 

USA: 40,000 lobotomized

???

Doświadczenie z osobowością lewej i prawej półkuli


mental illness was organic in nature, and reflected an underlying brain pathology

neurosurgeon, James W. Watts & neurologist Walter Freeman 

by 1951 over 18,608 individuals 

In the United States, approximately 40,000 people were lobotomized

By the 1970s, numerous countries had banned the procedure, as had several US states.


---

# Hemispherectomy

> Hemispherectomy is a very rare neurosurgical procedure in which a cerebral hemisphere (half of the brain) is removed, disconnected, or disabled.

"Clinical outcomes of hemispherectomy for epilepsy in childhood and adolescence" - A. M. Devlin, J. H. Cross, W. Harkness, W. K. Chong, B. Harding, F. Vargha‐Khadem, B. G. R. Neville; Brain, Volume 126, Issue 3, March 2003, Pages 556–566, https://doi.org/10.1093/brain/awg052

--

> Results: Mean age at surgery was 7.2 years. At follow-up, on average 5.4 years after surgery, 65% are seizure free, 49% are medication free, and, of those responding, none rated quality of life as worse than before surgery.

> Regardless of etiology, most patients showed only moderate change in cognitive performance at follow-up.

"The cognitive outcome of hemispherectomy in 71 children" - Margaret B Pulsifer 1, Jason Brandt, Cynthia F Salorio, Eileen P G Vining, Benjamin S Carson, John M Freeman

???

Peter Watts
"Zadziwiające że usunięcie połowy mózgu nie wydaje się znacząco wpływać na IQ ani na umiejętności motoryczne"

Switching vision with smell in mice

---

layout: false
background-image: url(img/brainTMS.png)
background-size: contain
background-size: 70% auto
background-repeat: no-repeat

## Consciousness only narrates made decisions

???

Professor Alvaro Pascual-Leone at Harvard

> They used transcranial magnetic stimulation (TMS), which discharges a magnetic pulse and excites the area of the brain
underneath, to stimulate the motor cortex and initiate movement in either the left or right hand. 

> Even after an experimenter manipulates a choice by stimulating the brain, participants often claim that their decision was freely chosen.

---

layout: false
background-image: url(img/splitperson.jpg)
background-size: contain
background-size: 50% auto
background-repeat: no-repeat
background-position: center right 

# Dissociative identity disorder

otherwise know as split personality

???

Zaburzenie dysocjacyjne tożsamości

Osobowość mnoga

---

# Weak hypothesis

Since

> Splitting the brain gives us two different persons

> "Human" software can run on limited hardware just fine

> Decisions are made before the concsiouc voice knows them

> There can be many personalities within a single brain

--

Therefore

> You have a team in your head

> Your conscious voice is the negiotiator (integrity)

> Learning to communicate may help you with yourself

---

layout: false
background-image: url(img/brain.jpeg)
background-size: contain
background-size: auto 100%
background-repeat: no-repeat
background-position: center right 

# More about brain

contact me at: jakubn@gmail.com

twitter: @jnabrdalik